﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace AlwaysWatchingBot
{
    // https://discord.foxbot.me/stable/guides/introduction/intro.html
    // I am using instances instead of dep injection to keep it simple for now. It's only a console app. Also helps keep libs a bit more portable.
    class Program
    {
        public static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        private DiscordSocketClient _client;

        private IConfigurationRoot _configuration;

        public async Task MainAsync()
        {
            try
            {
                // Setup configuration.
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                _configuration = builder.Build();

                // Setup logger and DB.
                Common.Logger.FilePath = _configuration["logFilePath"];
                Data.Repositories.BaseRepository.FilePath = _configuration["sqliteFilePath"];

                if (Data.Repositories.BaseRepository.SetupDbIfRequired())
                {
                    await TeeLogToConsoleAndFile("Created DB as SQLite file was missing.");
                }

                await TeeLogToConsoleAndFile("Starting always watching bot...");

                _client = new DiscordSocketClient();

                _client.Log += LogAsync;

                _client.MessageReceived += MessageReceived;

                //_client.Ready += () =>
                //{
                //    Console.WriteLine("Bot is connected!");
                //    return Task.CompletedTask;
                //};

                await _client.LoginAsync(TokenType.Bot, _configuration["discordToken"]);

                await _client.StartAsync();

                // Stop program exiting. Could also use a Console.ReadKey here.
                await Task.Delay(-1);
            }
            catch (Exception ex)
            {
                await TeeLogToConsoleAndFile("Exception occured. Message: " + ex.Message);
                Console.WriteLine("Press q to quit.");
                Console.ReadKey();
            }
        }

        private async Task LogAsync(LogMessage msg)
        {
            await TeeLogToConsoleAndFile(msg.ToString());
        }

        private async Task TeeLogToConsoleAndFile(string message)
        {
            Console.WriteLine(message);
            await Common.Logger.LogAsync(message);
        }

        private async Task LogToDB(SocketMessage message)
        {
            // Save message.
            var messageRepo = new Data.Repositories.MessageRepository();
            Data.Models.MessageModel messageModel = new Data.Models.MessageModel();
            messageModel.Id = message.Id.ToString();
            messageModel.DateTimeStamp = DateTime.UtcNow;
            messageModel.Author = message.Author.Username;

            // If the message is a PM it wont have a Guild so do a null check and cast type.
            SocketGuildChannel concreteChannel = message.Channel as SocketGuildChannel;
            if (concreteChannel != null)
            {
                messageModel.Server = concreteChannel.Guild.Name;
            }

            messageModel.Channel = message.Channel.Name;
            messageModel.Content = message.Content;

            await messageRepo.CreateMessageAsync(messageModel);

#if DEBUG
            string messageString = "DEBUG: Message recieved. " + " Author: " + messageModel.Author + " Server: " +
                messageModel.Server + " Channel: " + messageModel.Channel + " Content: " + messageModel.Content;
            Console.WriteLine(messageString);
#endif

            // Save Attachment.
            var attachmentRepo = new Data.Repositories.AttachmentRepository();
            if (message.Attachments.Count > 0)
            {
                foreach (Attachment attachment in message.Attachments)
                {
                    Data.Models.AttachmentModel attachmentModel = new Data.Models.AttachmentModel();
                    attachmentModel.Id = attachment.Id.ToString();
                    attachmentModel.MessageId = messageModel.Id;
                    attachmentModel.Url = attachment.Url;
                    await attachmentRepo.CreateAttachmentAsync(attachmentModel);

#if DEBUG
                    string attachmentString = "DEBUG: Message contained attachment. URL: " + attachmentModel.Url;
                    Console.WriteLine(attachmentString);
#endif
                }
            }
        }

        private async Task MessageReceived(SocketMessage message)
        {
            // Log all messages. Even log our own responses.
            await LogToDB(message);

            // Process commands.
            if (message.Content == "!ping")
            {
                await message.Channel.SendMessageAsync("Pong!");
            }
            else if(message.Content == "!systemuptime")
            {
                long ticks = Stopwatch.GetTimestamp();
                double uptime = ((double)ticks) / Stopwatch.Frequency;
                TimeSpan uptimeSpan = TimeSpan.FromSeconds(uptime);

                await message.Channel.SendMessageAsync(
                    "Uptime (dd:hh:mm): " + uptimeSpan.ToString("dd\\:hh\\:mm")
                );
            }
        }
    }
}

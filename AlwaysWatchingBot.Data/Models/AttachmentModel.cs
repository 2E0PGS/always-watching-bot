﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlwaysWatchingBot.Data.Models
{
    public class AttachmentModel
    {
        public string Id { get; set; }
        public string MessageId { get; set; }
        public string Url { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlwaysWatchingBot.Data.Models
{
    public class MessageModel
    {
        public string Id { get; set; }
        public DateTime DateTimeStamp { get; set; }
        public string Server { get; set; }
        public string Channel { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
    }
}

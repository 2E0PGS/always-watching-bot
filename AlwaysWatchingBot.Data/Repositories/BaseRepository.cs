﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace AlwaysWatchingBot.Data.Repositories
{
    public class BaseRepository
    {
        public static string FilePath { get; set; }

        public static SQLiteConnection GetSQLiteConnection()
        {
            if (string.IsNullOrEmpty(FilePath))
            {
                throw new ArgumentNullException("FilePath is empty. Did you setup this at startup?");
            }
            return new SQLiteConnection("Data Source=" + FilePath + "; Version=3;");
        }

        public static bool SetupDbIfRequired()
        {
            // Based on: https://bitbucket.org/2E0PGS/program-trak/src/master/ProgramTrak/Program.cs
            // VarChar SQlite limit: https://www.sqlite.org/faq.html#:~:text=(9)%20What%20is%20the%20maximum,all%20500%2Dmillion%20characters%20intact.
            // Discord recons its max message length is 2000
            // No DB file then make a new one.
            if (!File.Exists(FilePath))
            {
                SQLiteConnection.CreateFile(FilePath);

                // Setup Messages table.
                using (SQLiteConnection sqliteConnection = GetSQLiteConnection())
                {
                    sqliteConnection.Open();
                    string query = "CREATE TABLE Messages(Id VARCHAR(100), DateTimeStamp datetime, Server VARCHAR(100), Channel VARCHAR(100), Author VARCHAR(100), Content VARCHAR(2000))";
                    sqliteConnection.Execute(query);

                    // Setup Attachments table.
                    string query2 = "CREATE TABLE Attachments(Id VARCHAR(100), MessageId VARCHAR(100), Url VARCHAR(100))";
                    sqliteConnection.Execute(query2);

                    sqliteConnection.Close();
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}

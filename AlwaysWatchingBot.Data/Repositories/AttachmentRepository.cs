﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using AlwaysWatchingBot.Data.Models;
using System.Threading.Tasks;

namespace AlwaysWatchingBot.Data.Repositories
{
    public class AttachmentRepository : BaseRepository
    {
        public async Task CreateAttachmentAsync(AttachmentModel attachmentModel)
        {
            using (SQLiteConnection sqliteConnection = GetSQLiteConnection())
            {
                sqliteConnection.Open();
                string query = "INSERT INTO Attachments (Id, MessageId, Url) Values (@Id, @MessageId, @Url)";
                int rows = await sqliteConnection.ExecuteAsync(query, attachmentModel);
                sqliteConnection.Close();
                if (rows != 1)
                {
                    throw new Exception("Effected row count was an unexpected: " + rows);
                }
            }
        }
    }
}

﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using AlwaysWatchingBot.Data.Models;
using System.Threading.Tasks;

namespace AlwaysWatchingBot.Data.Repositories
{
    public class MessageRepository : BaseRepository
    {
        public async Task CreateMessageAsync(MessageModel messageModel)
        {
            using (SQLiteConnection sqliteConnection = GetSQLiteConnection())
            {
                sqliteConnection.Open();
                string query = "INSERT INTO Messages (Id, DateTimeStamp, Server, Channel, Author, Content) Values (@Id, @DateTimeStamp, @Server, @Channel, @Author, @Content)";
                int rows = await sqliteConnection.ExecuteAsync(query, messageModel);
                sqliteConnection.Close();
                if (rows != 1)
                {
                    throw new Exception("Effected row count was an unexpected: " + rows);
                }
            }
        }
    }
}

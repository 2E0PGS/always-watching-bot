﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace AlwaysWatchingBot.Common
{
    public static class Logger
    {
        public static string FilePath { get; set; }

        public static async Task LogAsync(string message)
        {
            await AppendToLogFileAsync(message);
        }

        private static async Task AppendToLogFileAsync(string message)
        {
            if (string.IsNullOrEmpty(FilePath))
            {
                throw new ArgumentNullException("FilePath is empty. Did you setup this at startup?");
            }
            using (StreamWriter streamWriter = File.AppendText(FilePath))
            {
                string dateTimeStamp = DateTime.UtcNow.ToString("s", System.Globalization.CultureInfo.InvariantCulture);
                streamWriter.WriteLine(dateTimeStamp + ": " + message);
                await streamWriter.FlushAsync();
                streamWriter.Close();
            }
        }
    }
}
